//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Author: Matthew Hoppesch

#ifndef ModTypeLHCf_h
#define ModTypeLHCf_h 1

#include "globals.hh"
#include "G4PVPlacement.hh"
#include "Materials.hh"

#include <vector>

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4VSolid;
class G4Material;

/// Detector construction class to define materials and geometry.

class ModTypeLHCf
{
public:
  ModTypeLHCf(const int, ModTypeLHCf*);
  ModTypeLHCf(const int, G4LogicalVolume*, G4ThreeVector* );
  ~ModTypeLHCf();

  virtual void  Construct();
  virtual void  ConstructSDandField();

  virtual void  ConstructDetector();


  inline  void  SetPosition          ( G4ThreeVector* vec ){ delete m_pos;       m_pos       = vec; }
  inline  void  SetOpticalFlag       ( G4bool         arg ){ OPTICAL             = arg; }
  inline  void  SetOverlapsFlag      ( G4bool         arg ){ CHECK_OVERLAPS      = arg; }
  inline  void  SetReducedTreeFlag   ( G4bool         arg ){ REDUCED_TREE        = arg; }
  inline  void  SetWThickness        ( G4double       arg ){ m_WThickness  = arg; }
  inline  void  SetSiThickness       ( G4double       arg ){ m_SiThickness  = arg; }
  inline  void  SetGSOThickness      ( G4double       arg ){ m_GSOThickness  = arg; }




  inline  G4LogicalVolume* GetWtLogicalVolume( ){ return m_WtLogical;    }
  inline  G4LogicalVolume* GetGSOtLogicalVolume( ){ return m_GSOtLogical;    }
  inline  G4LogicalVolume* GetSiLogicalVolume( ){ return m_SiLogical;    }
  inline  G4ThreeVector*   GetPosition             ( ){ return m_pos;         }
  inline  G4int            GetnFibers              ( ){ return m_numPlates;     }
  inline  G4int            GetModNum               ( ){ return m_modNum;      }
  inline  G4bool           GetOpticalFlag          ( ){ return OPTICAL;       }
  inline  G4bool           GetReducedTreeFlag      ( ){ return REDUCED_TREE;  }

protected:
  const G4int      m_modNum;
  G4int            m_numPlates;
  G4ThreeVector*   m_pos;
  G4double         m_WThickness;
  G4double         m_GSOThickness;
  G4double         m_SiThickness;
  std::vector< G4double > m_topOfVolume;


  G4bool           OPTICAL;
  G4bool           CHECK_OVERLAPS;
  G4bool           REDUCED_TREE;
  G4Material*      m_Wmat;
  G4Material*      m_Simat;
  G4Material*      m_GSOmat;
  G4Material*      m_WVguidemat;
  G4Material*      m_foilmat;
  Materials*       m_materials;
  G4LogicalVolume* m_logicMother;


protected:

  G4VSolid*          m_HousingBox;
  G4LogicalVolume*   m_HousingLogical;
  G4VPhysicalVolume* m_HousingPhysical;

  G4VSolid*          m_ModuleBox;
  G4LogicalVolume*   m_ModuleLogical;
  G4VPhysicalVolume* m_ModulePhysical;


  G4VSolid*          m_Wt;
  G4LogicalVolume*   m_WtLogical;
  std::vector < G4VPhysicalVolume* > m_WtPhysical;

  G4VSolid*          m_Wb;
  G4LogicalVolume*   m_WbLogical;
  std::vector < G4VPhysicalVolume* > m_WbPhysical;

  G4VSolid*          m_GSOt;
  G4LogicalVolume*   m_GSOtLogical;
  std::vector < G4VPhysicalVolume* > m_GSOtPhysical;

  G4VSolid*          m_GSOtd;
  G4LogicalVolume*   m_GSOtdLogical;
  std::vector < G4VPhysicalVolume* > m_GSOtdPhysical;

  G4VSolid*          m_GSOtfoil;
  G4LogicalVolume*   m_GSOtfoilLogical;
  std::vector < G4VPhysicalVolume* > m_GSOtfoilPhysical;

  G4VSolid*          m_GSOb;
  G4LogicalVolume*   m_GSObLogical;
  std::vector < G4VPhysicalVolume* > m_GSObPhysical;

  G4VSolid*          m_GSObd;
  G4LogicalVolume*   m_GSObdLogical;
  std::vector < G4VPhysicalVolume* > m_GSObdPhysical;

  G4VSolid*          m_GSObfoil;
  G4LogicalVolume*   m_GSObfoilLogical;
  std::vector < G4VPhysicalVolume* > m_GSObfoilPhysical;

  G4VSolid*          m_Sid;
  G4LogicalVolume*   m_SidLogical;
  std::vector < G4VPhysicalVolume* > m_SidPhysical;

  G4VSolid*          m_Si;
  G4LogicalVolume*   m_SiLogical;
  std::vector < G4VPhysicalVolume* > m_SiPhysical;

  G4VSolid*          m_PMMAt;
  G4LogicalVolume*   m_PMMAtLogical;
  std::vector < G4VPhysicalVolume* > m_PMMAtPhysical;
  
  G4VSolid*          m_PMMAb;
  G4LogicalVolume*   m_PMMAbLogical;
  std::vector < G4VPhysicalVolume* > m_PMMAbPhysical;

  // Vertical quartz strips (these strips are full -- no partial segments)

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
