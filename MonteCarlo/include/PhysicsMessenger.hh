// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file PhysicsMessenger.hh
/// \brief Definition of the PhysicsMessenger class
/// \author Chad Lantz
/// \date 16 April 2020

#ifndef PhysicsMessenger_h
#define PhysicsMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class PhysicsList;
class G4UIdirectory;
class G4UIcommand;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithABool;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithoutParameter;
class G4UIcmdWith3VectorAndUnit;


class PhysicsMessenger: public G4UImessenger{
  public:

    PhysicsMessenger(PhysicsList* );
   ~PhysicsMessenger();

    virtual void     SetNewValue     (G4UIcommand*, G4String);
    virtual G4String GetCurrentValue (G4UIcommand * command);

  private:

    PhysicsList*               fPhysicsList;

    //Directories
    G4UIdirectory*             fPhysicsDir;

    //Commands
    G4UIcmdWithAString*        fSelectListCmd;
    G4UIcmdWithADoubleAndUnit* fsetCutCmd;
    G4UIcommand*               fsetCutRCmd;
    G4UIcommand*               fsetCutForAGivenParticleCmd;
    G4UIcmdWithAString*        fgetCutForAGivenParticleCmd;
    G4UIcmdWithAnInteger*      fverboseCmd;
    G4UIcmdWithoutParameter*   fdumpListCmd;
    G4UIcmdWithAString*        faddProcManCmd;
    G4UIcmdWithAString*        fbuildPTCmd;
    G4UIcmdWithAString*        fstoreCmd;
    G4UIcmdWithAString*        fretrieveCmd;
    G4UIcmdWithAnInteger*      fasciiCmd;
    G4UIcommand*               fapplyCutsCmd;
    G4UIcmdWithAString*        fdumpCutValuesCmd;
    G4UIcmdWithAnInteger*      fdumpOrdParamCmd;

};
#endif
