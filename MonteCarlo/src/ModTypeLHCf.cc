//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \ingroup mc
/// \file ModTypeLHCf.cc
/// \author Matthew Hoppesch
/// \brief LHCf detector construction
#include "ModTypeLHCf.hh"
#include "FiberSD.hh"

#include "G4GeometryManager.hh"
#include "G4SolidStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4MaterialTable.hh"

#include "G4SDManager.hh"

#include "G4Box.hh"
#include "G4Para.hh"
#include "G4Tubs.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include <stdio.h>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ModTypeLHCf::ModTypeLHCf(const int cn, G4LogicalVolume* mother, G4ThreeVector* pos)
  : m_modNum( cn ),
    m_pos( pos ),
    m_WThickness(5.0*mm),
    m_GSOThickness(5.0*mm),
    m_SiThickness(5.0*mm),
    OPTICAL(false),
    CHECK_OVERLAPS(false),
    REDUCED_TREE(false),
    m_logicMother( mother )
{
	m_materials = Materials::getInstance();
  m_materials->UseOpticalMaterials(true);
  m_materials->DefineOpticalProperties();
	m_Wmat = m_materials->NiW;
	m_Simat  = m_materials->pureSi;
  m_GSOmat = m_materials->GSO;
  m_WVguidemat = m_materials->PMMA;
  m_foilmat = m_materials->Al;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ModTypeLHCf::ModTypeLHCf(const int cn, ModTypeLHCf* right)
	: m_modNum( cn )
{
		m_pos				 			 = new G4ThreeVector(*right->m_pos);
		m_WThickness = right->m_WThickness;
    m_GSOThickness = right->m_GSOThickness;
    m_SiThickness = right->m_SiThickness;
		OPTICAL 					 = right->OPTICAL;
		CHECK_OVERLAPS 		 = right->CHECK_OVERLAPS;
		REDUCED_TREE 		   = right->REDUCED_TREE;
		m_logicMother			 = right->m_logicMother;
    m_materials = Materials::getInstance();
    m_materials->UseOpticalMaterials(true);
    m_materials->DefineOpticalProperties();
  	m_Wmat = m_materials->NiW;
  	m_Simat  = m_materials->pureSi;
    m_GSOmat = m_materials->GSO;
    m_WVguidemat = m_materials->PMMA;
    m_foilmat = m_materials->Al;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ModTypeLHCf::~ModTypeLHCf()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ModTypeLHCf::Construct(){
  ConstructDetector();
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ModTypeLHCf::ConstructDetector()
{
	// geometric constants
  float zStartW; 		 // position where first tungsten plate gets placed
  std::vector<int> order{1,2,1,2,1,3,2,1,2,1,2,1,3,2,1,2,1,2,1,3,2,1,2,1,2,1,3,1,2,1,1,2,1,3,1,2,1,1,2,1,3,1,2,1};
	float boxWidthX  = 66*mm; //XYZ of volume in which LHCf is built
	float boxHeightY = 66*mm;
  float boxLengthZ = 210*mm;
  float pushlen=1.*mm; //variable to keep track of distnace for stacking plates
  float tplate = 32*mm; //sq dim of top plate
  float bplate = 25*mm; //sq dim of bot plate
  float topp = 9*mm;  //trapazodial detector z-dimenson/2 top and bottom
  float botp= 7*mm;
  float bdheight = 34*mm;
  float Sip= 100*mm; //Si paddle height
  float case_width = 20*mm;
  float buffer= 5 * mm;
  float foil_thickness = .1*mm;
  zStartW=boxLengthZ*mm/2.0;
  m_numPlates=0;

  //----------------------------------------------
  // Housing
  //----------------------------------------------

  m_HousingBox     = new G4Box("SteelCasing",  boxWidthX*mm/2.0 + case_width/2. ,boxHeightY*mm/2.0 + bplate + case_width/2. , boxLengthZ*mm/2.0+case_width/2.);
  m_HousingLogical = new G4LogicalVolume(m_HousingBox           ,m_materials->Al, 		 "Housing_Logical");
  m_ModuleBox      = new G4Box("ModuleCasing", boxWidthX*mm/2.0 + buffer,boxHeightY*mm/2.0 + bplate + buffer, boxLengthZ*mm/2.0 + buffer);
  m_ModuleLogical  = new G4LogicalVolume(m_ModuleBox            ,m_materials->Air, "Module_Logical");

  char name[256];
  int cn = m_modNum;
  sprintf(name,"LHCf%d_Case_Physical", m_modNum);
  G4RotationMatrix* nullRotation = new G4RotationMatrix();
  G4RotationMatrix* Rotation = new G4RotationMatrix();
  Rotation->rotateX(270.*deg);
  G4ThreeVector  pos;
  pos = G4ThreeVector(0,0,0);
  m_HousingPhysical = new G4PVPlacement(nullRotation,G4ThreeVector( m_pos->x(), m_pos->y(), m_pos->z() ) ,m_HousingLogical,name,m_logicMother,false,cn,CHECK_OVERLAPS);
  m_ModulePhysical = new G4PVPlacement(nullRotation,pos ,m_ModuleLogical,name,m_HousingLogical,false,cn,CHECK_OVERLAPS);

  G4VisAttributes* housingColor = new G4VisAttributes( );
	housingColor->SetColor(1.,1.,1.,.2);
  m_HousingLogical->SetVisAttributes( housingColor );

  G4VisAttributes* moduleColor = new G4VisAttributes( );
	moduleColor->SetColor(1.,0.,0.,.0);
  m_ModuleLogical->SetVisAttributes( moduleColor );
  //----------------------------------------------
  // Tungsten Plates
  //----------------------------------------------

  m_Wt = new G4Box("Wt",
									tplate/2.0 ,
									tplate/2.0,
									m_WThickness/2.0);
  m_WtLogical =
		new G4LogicalVolume(m_Wt,
												m_Wmat,
												"Wt_Logical");
  m_GSOt = new G4Box("GSOt",
  									tplate/2.0 ,
  									tplate/2.0,
  									m_GSOThickness/2.0);
  m_GSOtLogical =
  		new G4LogicalVolume(m_GSOt,
  												m_GSOmat,
  												"GSOt_Logical");
  m_GSOtd = new G4Trd("GSOtd",
  									topp ,
  									tplate/2.0,
                    m_GSOThickness/2.0,
                    m_GSOThickness/2.0,
  									bplate/2.);
  m_GSOtdLogical =
  		new G4LogicalVolume(m_GSOtd,
  												m_WVguidemat,
  												"GSOtd_Logical");
  m_GSOtfoil = new G4Trd("GSOtfoil",
  									tplate/4.0 + foil_thickness,
  									tplate/2.0 + foil_thickness,
                    m_GSOThickness/2.0 + foil_thickness,
                    m_GSOThickness/2.0 + foil_thickness,
  									bplate/2.);
  m_GSOtfoilLogical =
  		new G4LogicalVolume(m_GSOtfoil,
  												m_foilmat,
  												"GSOtfoil_Logical");
  m_Wb = new G4Box("Wb",
                        bplate/2.0 ,
                        bplate/2.0,
                        m_WThickness/2.0);
  m_WbLogical =
   new G4LogicalVolume(m_Wb,
                        m_Wmat,
                        "Wb_Logical");
  m_GSOb = new G4Box("GSOb",
                        bplate/2.0 ,
                        bplate/2.0,
                        m_GSOThickness/2.0);
  m_GSObLogical =
     new G4LogicalVolume(m_GSOb,
                         m_GSOmat,
                         "GSOb_Logical");
  m_GSObd = new G4Trd("GSObd",
  									botp ,
  									bplate/2.0,
                    m_GSOThickness/2.0,
                    m_GSOThickness/2.0,
  									bdheight/2.);
  m_GSObdLogical =
     new G4LogicalVolume(m_GSObd,
                         m_WVguidemat,
                         "GSObd_Logical");
  m_GSObfoil = new G4Trd("GSObfoil",
  									7*mm + foil_thickness,
  									bplate/2.0 + foil_thickness,
                    m_GSOThickness/2.0 + foil_thickness,
                    m_GSOThickness/2.0 + foil_thickness,
  									bdheight/2.);
  m_GSObfoilLogical =
     new G4LogicalVolume(m_GSObfoil,
                         m_foilmat,
                         "GSObfoil_Logical");
  m_Si = new G4Box("Si",
                          57.*mm/2.0 ,
                          57*mm/2.0 ,
                          m_SiThickness/2.0);
  m_SiLogical =
          new G4LogicalVolume(m_Si,
                          m_Simat,
                          "Si_Logical");
  m_Sid = new G4Box("Sid",
                          57.*mm/2.0 ,
                          Sip/2.0 ,
                          m_SiThickness/2.0);
  m_SidLogical =
          new G4LogicalVolume(m_Sid,
                          m_WVguidemat,
                          "Sid_Logical");
  m_PMMAt = new G4Box("PMMAt",
                          tplate/2.0 ,
                          tplate/2.0 ,
                          m_GSOThickness/2.0);
  m_PMMAtLogical =
          new G4LogicalVolume(m_PMMAt,
                          m_WVguidemat,
                          "PMMAt_Logical");
  m_PMMAb = new G4Box("PMMAb",
                        bplate/2.0 ,
                        bplate/2.0 ,
                        m_GSOThickness/2.0);
  m_PMMAbLogical =
        new G4LogicalVolume(m_PMMAb,
                        m_WVguidemat,
                        "PMMAb_Logical");

  G4VisAttributes* WColor = new G4VisAttributes();
  G4VisAttributes* GSOColor = new G4VisAttributes();
  G4VisAttributes* SiColor = new G4VisAttributes( );
  G4VisAttributes* SidColor = new G4VisAttributes( );
  WColor->SetColor(.5,.5,.5,1.);
  GSOColor->SetColor(0.,1.,1.,1.);
  SiColor->SetColor(.718,.255,.055,1.);
  SidColor->SetColor(1.,.86,.345,1.);
  //WColor->SetForceSolid(true);
  //GSOColor->SetForceSolid(true);
  //SiColor->SetForceSolid(true);
  m_WtLogical->SetVisAttributes( WColor );
  m_GSOtLogical->SetVisAttributes(GSOColor);
  m_GSOtdLogical->SetVisAttributes(GSOColor);
  m_WbLogical->SetVisAttributes( WColor );
  m_GSObLogical->SetVisAttributes(GSOColor);
  m_GSObdLogical->SetVisAttributes(GSOColor);
  m_GSObfoilLogical->SetVisAttributes(housingColor);
  m_GSOtfoilLogical->SetVisAttributes(housingColor);
  m_SiLogical->SetVisAttributes(SiColor);
  m_SidLogical->SetVisAttributes(SidColor);
  m_PMMAbLogical->SetVisAttributes(SiColor);
  m_PMMAtLogical->SetVisAttributes(SiColor);
  //  W plates (non pixel modules): Physical plates that span length of each absorber gap
  // 11 layers of plates

  for(int i = 0; i<(int)(order.size()); i++){
    sprintf(name,"LHCf%d_Component%d",m_modNum, i);
   if(order.at(i)==1){
     sprintf(name,"LHCf%d_W%d",m_modNum, (int)(m_WtPhysical.size()+1));
    m_WtPhysical.push_back( new G4PVPlacement(nullRotation,
													 G4ThreeVector(tplate/2.0,tplate/2.0,zStartW-pushlen-m_WThickness/2.0),
													 m_WtLogical,
													 name,
													 m_ModuleLogical,
													 false,
													 cn,
													 CHECK_OVERLAPS) );
    m_WbPhysical.push_back( new G4PVPlacement(nullRotation,
                          G4ThreeVector(-bplate/2.0,-bplate/2.0,zStartW-pushlen-m_WThickness/2.0),
                          m_WbLogical,
                          name,
                          m_ModuleLogical,
                          false,
                          cn,
                          CHECK_OVERLAPS) );
                           pushlen += m_WThickness;}
   else if(order.at(i)==2){
     sprintf(name,"LHCf%d_GSO%d",m_modNum, (int)(m_GSOtPhysical.size()+1));
    m_PMMAtPhysical.push_back( new G4PVPlacement(nullRotation,
                          G4ThreeVector(tplate/2.0,tplate/2.0,zStartW-pushlen-m_GSOThickness/2.0),
                          m_PMMAtLogical,
                          name,
                          m_ModuleLogical,
                          false,
                          cn,
                          CHECK_OVERLAPS) );
    m_PMMAbPhysical.push_back( new G4PVPlacement(nullRotation,
                          G4ThreeVector(-bplate/2.0,-bplate/2.0,zStartW-pushlen-m_GSOThickness/2.0),
                          m_PMMAbLogical,
                          name,
                          m_ModuleLogical,
                          false,
                          cn,
                          CHECK_OVERLAPS) );
                          pushlen += m_GSOThickness;
    m_GSOtPhysical.push_back( new G4PVPlacement(nullRotation,
                           G4ThreeVector(tplate/2.0,tplate/2.0,zStartW-pushlen-m_GSOThickness/2.0),
                           m_GSOtLogical,
                           name,
                           m_ModuleLogical,
                           false,
                           cn,
                           CHECK_OVERLAPS) );
    m_GSOtfoilPhysical.push_back( new G4PVPlacement(Rotation,
                           G4ThreeVector(tplate/2,tplate+bplate/2.,zStartW-pushlen-m_GSOThickness/2.0),
                           m_GSOtfoilLogical,
                           name,
                           m_ModuleLogical,
                           false,
                           cn,
                           CHECK_OVERLAPS) );
    m_GSOtdPhysical.push_back(new G4PVPlacement(nullRotation,
                           pos,
                           m_GSOtdLogical,
                           name,
                           m_GSOtfoilLogical,
                           false,
                           cn,
                           CHECK_OVERLAPS) );
    m_GSObPhysical.push_back( new G4PVPlacement(nullRotation,
                           G4ThreeVector(-bplate/2.0,-bplate/2.0,zStartW-pushlen-m_GSOThickness/2.0),
                           m_GSObLogical,
                           name,
                           m_ModuleLogical,
                           false,
                           cn,
                           CHECK_OVERLAPS) );
    m_GSObfoilPhysical.push_back( new G4PVPlacement(Rotation,
                           G4ThreeVector(-bplate/2.0,bdheight/2.,zStartW-pushlen-m_GSOThickness/2.0),
                           m_GSObfoilLogical,
                           name,
                           m_ModuleLogical,
                           false,
                           cn,
                           CHECK_OVERLAPS) );
    m_GSObdPhysical.push_back(new G4PVPlacement(nullRotation,
                           pos,
                           m_GSObdLogical,
                           name,
                           m_GSObfoilLogical,
                           false,
                           cn,
                           CHECK_OVERLAPS) );
                           pushlen += m_GSOThickness;
   m_PMMAtPhysical.push_back( new G4PVPlacement(nullRotation,
                         G4ThreeVector(tplate/2.0,tplate/2.0,zStartW-pushlen-m_GSOThickness/2.0),
                         m_PMMAtLogical,
                         name,
                         m_ModuleLogical,
                         false,
                         cn,
                         CHECK_OVERLAPS) );
   m_PMMAbPhysical.push_back( new G4PVPlacement(nullRotation,
                         G4ThreeVector(-bplate/2.0,-bplate/2.0,zStartW-pushlen-m_GSOThickness/2.0),
                         m_PMMAbLogical,
                         name,
                         m_ModuleLogical,
                         false,
                         cn,
                         CHECK_OVERLAPS) );
                         pushlen += m_GSOThickness;
                           m_numPlates++;}

  else if(order.at(i)==3){
    sprintf(name,"LHCf%d_Si%d",m_modNum, (int)(m_SiPhysical.size()+1));
    /*m_SiPhysical.push_back( new G4PVPlacement(nullRotation,
													 G4ThreeVector(3.5*mm,3.5*mm,zStartW-pushlen-m_SiThickness/2.0),
													 m_SiLogical,
													 name,
													 m_ModuleLogical,
													 false,
													 cn,
													 CHECK_OVERLAPS) );*/
    /*m_SidPhysical.push_back( new G4PVPlacement(nullRotation,
													 G4ThreeVector(3.5*mm,tplate+Sip/2.,zStartW-pushlen-m_SiThickness/2.0),
													 m_SidLogical,
													 name,
													 m_ModuleLogical,
													 false,
													 cn,
													 CHECK_OVERLAPS) );*/
                           pushlen += m_SiThickness;}
  else{std::cout<<"ERROR: ORDERING ARRAY IS NOT CORRECT"<< std::endl;}
   }



  m_topOfVolume.push_back(m_pos->y()+bdheight);
  m_topOfVolume.push_back(m_pos->y()+bplate+tplate);


  std::cout << "ModTypeLHCf construction finished: Module Number " << m_modNum << std::endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ModTypeLHCf::ConstructSDandField(){
G4SDManager* SDman = G4SDManager::GetSDMpointer();
std::cout << "NumPlate ==== "<< m_numPlates << std::endl;
char SDname[256];
sprintf( SDname, "LHCf%d_SDb", m_modNum);
FiberSD* bSD = new FiberSD( SDname, m_modNum, OPTICAL );
bSD->HistInitialize();
bSD->SetTopOfVolume( m_topOfVolume.at(0) );
SDman->AddNewDetector( bSD );

if(REDUCED_TREE){
  bSD->SetReducedTree( m_numPlates, 1 );
}

m_GSObdLogical->SetSensitiveDetector( bSD );

//===============

sprintf( SDname, "LHCf%d_SDt", m_modNum);
FiberSD* tSD = new FiberSD( SDname, m_modNum, OPTICAL );
tSD->HistInitialize();
tSD->SetTopOfVolume( m_topOfVolume.at(1) );
SDman->AddNewDetector( tSD );

if(REDUCED_TREE){
  tSD->SetReducedTree( m_numPlates, 1 );
}

m_GSOtdLogical->SetSensitiveDetector( tSD );
std::cout << "LHCf SD construction finished: SD name " << SDname << std::endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
